#!/bin/bash
set -e


function doc() {
    cat <<EOF

********
play scenario:

committing changes with multiline commit message
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}
TIMESTAMP=$(date +"%Y.%m.%d_%H.%M.%S")

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir trunk
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

echo $TIMESTAMP > trunk/foobar1.txt
svn add trunk/foobar1.txt
svn ci -m $'\ncommit message line1\ncommit message line2\ncommit message line3\n'

svn up && svn log -vl1 .

echo "
multiline
commit
message
in separated
file
" > ci_msg.txt

echo $TIMESTAMP > trunk/foobar2.txt

svn add trunk/foobar2.txt
svn ci -F ci_msg.txt

svn up && svn log -vl1 .

