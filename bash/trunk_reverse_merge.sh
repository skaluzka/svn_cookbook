#!/bin/bash
set -e


function doc() {
    cat <<EOF

********
play scenario:

someone accidentally deleted "trunk/" dir! it will be restored with
so-called reverse merge operation which is based on svn merge command
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}


echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

#committing some changes to trunk...
cd $SVN_WORKSPACE/trunk
FILES="file1.txt file2.txt file3.txt"
for f in $FILES; do
    date > $f
    svn add $f
    svn st
    svn ci -m "file $f added to trunk"
done
svn st && svn up && svn log -v .

#creating tmp/ dir
mkdir -p $SVN_WORKSPACE/tmp

#silly backup for diff purposes (part 1)
cp -vr $SVN_WORKSPACE/trunk $SVN_WORKSPACE/tmp/r4_trunk

#deleting trunk...
echo "warning! deleting trunk..."
cd $SVN_WORKSPACE
svn del trunk/
svn ci -m "\"copy/paste error\" :)"
svn up && svn log -v .

#let's do the reverse merge now...
svn merge -r5:4 .
svn st
svn ci -m "trunk resurrection"
svn up

#silly backup for diff purposes (part 2)
cp -vr $SVN_WORKSPACE/trunk $SVN_WORKSPACE/tmp/r6_trunk

#do the diff
cd $SVN_WORKSPACE/tmp
diff -rs r*/

#svn log with --stop-on-copy
cd ..
svn log --stop-on-copy .

echo "done"

