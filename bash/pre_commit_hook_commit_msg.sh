#!/bin/bash
#set -e
#set -x


function doc() {
    cat <<EOF

********
play scenario:

simple pre-commit hook which checks whether the commit message is
empty or not
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags hooks
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

echo 'preparing pre commit hook script: "pre-commit" please wait...'
cd hooks
cat << EOF > pre-commit
#!/bin/bash

REPOS="\${1}"
TXN="\${2}"
SVNLOOK=/usr/bin/svnlook
COMMIT_MSG=\$(\${SVNLOOK} log -t \${TXN} \${REPOS})
VERBOSE=true
#VERBOSE=false

if [ "\${VERBOSE}" = true ] ; then
    echo "REPOS=\${REPOS}" >&2
    echo "TXN=\${TXN}" >&2
    echo "COMMIT_MSG=\"\${COMMIT_MSG}\"" >&2
    echo "changed paths:" >&2
    \${SVNLOOK} changed -t \${TXN} \${REPOS} >&2
fi

#now let's check the content of commit message and
#exit with error code 1 if it's empty

if [ -z \${COMMIT_MSG} ] ; then
    cat \${REPOS}/hooks/stop.txt >&2
    echo "empty commit message?" >&2
    exit 1
fi

#otherwise exit without errors
exit 0
EOF

chmod +x pre-commit
cat pre-commit
svn st
svn add pre-commit
svn ci -m"added pre-commit to hooks/ dir"
svn up && svn log -vl1 .

echo 'installing pre commit script "pre-commit" in repository, please wait...'
cp -v pre-commit ${SVN_REPO}/hooks/
cp -v ${WORK_DIR}/ascii_banners/stop.txt ${SVN_REPO}/hooks/
ls -al ${SVN_REPO}/hooks

cd ${SVN_WORKSPACE}/trunk
> foobar.txt
svn add foobar.txt
svn st
svn ci -m"            "
svn up && svn log -vl1 .
svn ci -m $'\n'
svn up && svn log -vl1 .
svn ci -m $'\t'
svn up && svn log -vl1 .
svn ci -m $'\n       \t  \t \n\n\n\t'
svn up && svn log -vl1 .

echo "done"

