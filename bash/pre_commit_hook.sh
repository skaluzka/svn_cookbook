#!/bin/bash
#set -e
#set -x


function doc() {
    cat <<EOF

********
play scenario:

simple pre-commit hook which blocks all commits to the repository
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags hooks
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

echo 'preparing pre commit hook script: "pre-commit" please wait...'
cd hooks
cat << EOF > pre-commit
#!/bin/bash

REPOS="\$1"
TXN="\$2"
PYTHON=\$(python -V)
SVNLOOK=/usr/bin/svnlook
COMMIT_MSG=\$(\${SVNLOOK} log -t \${TXN} \${REPOS})
VERBOSE=true

if [ "\${VERBOSE}" = true ] ; then
    echo "REPOS=\${REPOS}" >&2
    echo "TXN=\${TXN}" >&2
    echo "pwd=\$(pwd)" >&2
    echo "PYTHON=\${PYTHON}" >&2
    echo "BASH_VERSION=\${BASH_VERSION}" >&2
    echo "COMMIT_MSG=\${COMMIT_MSG}" >&2
    echo "modified paths:" >&2
    \${SVNLOOK} changed -t \${TXN} \${REPOS} >&2
fi

#always return with error
exit 1
EOF

chmod +x pre-commit
cat pre-commit
svn st
svn add pre-commit
svn ci -m"added pre-commit to hooks/ dir"
svn up && svn log -vl1 .

echo 'installing pre commit script "pre-commit" in repository, please wait...'
cp -v pre-commit ${SVN_REPO}/hooks/
ls -al ${SVN_REPO}/hooks

cd ${SVN_WORKSPACE}
> trunk/foobar.txt
svn add trunk/foobar.txt
svn st
svn ci trunk/foobar.txt -m "added foobar.txt"
echo "checking svn workspace status, please wait..."
svn up && svn log -vl1 .

echo "done"

