#!/bin/bash
set -e
set -x

function doc() {
    cat <<EOF

********
play scenario:

we want to know how to add and remove file with "@" character in the
name to the repository
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://"${SVN_REPO}" "${SVN_WORKSPACE}"

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

#adding and removing files with "@" in the name is a little
#bit tricky in svn;)
>"trunk/foobar666@txt"
svn add "trunk/foobar666@txt@"

svn st
svn ci -m "added trunk/foobar666@txt file"
svn up && svn log -vl1 .

#let's remove it now
svn del "trunk/foobar666@txt@"
svn st
svn ci -m "file trunk/foobar666@txt has been removed"
svn up && svn log -vl1 .

echo "done"

