#!/bin/bash
set -e

function doc() {
    cat <<EOF

********
play scenario:

breaking svn import (svn import is an atomic operation)
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}
TIMESTAMP=$(date +"%Y.%m.%d_%H.%M.%S")

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE BIN_FILES_${DIR_SUFFIX}

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

cd ..

mkdir -p BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP

echo "generating random files, please wait..."

echo "generating file_A_128B.bin file, please wait..."
dd if=/dev/urandom of=BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP/file_A_128B.bin bs=1 count=128 2>/dev/null
echo "file_A_128B.bin generated"

echo "generating file_B_1KB.bin file, please wait..."
dd if=/dev/urandom of=BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP/file_B_1KB.bin bs=1k count=1 2>/dev/null
echo "file_B_1KB.bin generated"

echo "generating file_C_64MB.bin file, please wait..."
dd if=/dev/urandom of=BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP/file_C_64MB.bin bs=1M count=64 2>/dev/null
echo "file_C_64MB.bin generated"

echo "generating file_D_128MB.bin file, please wait..."
dd if=/dev/urandom of=BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP/file_D_128MB.bin bs=1M count=128 2>/dev/null
echo "file_D_128MB.bin generated"

echo "random files generated"

echo "TIMESTAMP=$TIMESTAMP"
svn import BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP file://$SVN_REPO/tags/BIN_FILES_${DIR_SUFFIX}/$TIMESTAMP -m "importing bin files for timestamp: $TIMESTAMP" &
PID=$(ps a -u$USER | grep $TIMESTAMP | grep -v grep | awk '{print $1}')
#echo "PID=$PID"
sleep 2s
echo "killing process with pid: $PID"
kill $PID

cd $SVN_WORKSPACE
svn up
svn log -vl1 .

