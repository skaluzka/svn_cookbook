#!/bin/bash
set -e


function doc() {
    cat <<EOF

********
play scenario:

we need to checkout a single file. this is not supported in svn
because each checkout command can be done only for a directory.
however some clever combination of "svn co" and "svn up" commands
can be used for single file checkout
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}1
SVN_WORKSPACE2=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}2

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE $SVN_WORKSPACE2

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

cd $SVN_WORKSPACE/trunk
echo 'creating nested dir structure in "trunk" with foobar0.txt ... foobar127.txt files'
svn mkdir --parents dir1/dir2/dir3/dir4/dir5/dir6/dir7/dir8
touch dir1/dir2/dir3/dir4/dir5/dir6/dir7/dir8/foobar{000..999}.txt
svn add dir1/dir2/dir3/dir4/dir5/dir6/dir7/dir8/foobar*
svn ci -m "added dir1/dir2/dir3/dir4/dir5/dir6/dir7/dir8/foobar*.txt"
svn st && svn up && svn log -v .

#now we have 1000files in one directory but we want to checkout only
#one of them, for example foobar666.txt

mkdir -p $SVN_WORKSPACE2
cd $SVN_WORKSPACE2
svn co --depth=empty file://${SVN_REPO}/trunk/dir1/dir2/dir3/dir4/dir5/dir6/dir7/dir8 .
svn up foobar666.txt
ls -al
date > foobar666.txt
svn st
svn di
svn ci -m "timestamp added in foobar666.txt"
svn st && svn up && svn log -vl1 .

echo "done"

