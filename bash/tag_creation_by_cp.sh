#!/bin/bash
set -e
set -x


function doc() {
    cat <<EOF

********
play scenario:

we want to create a tag with svn copy command
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}


echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

#committing some changes to trunk...
FILES="file1.txt file2.txt file3.txt"
for f in $FILES; do
    date > trunk/$f
    svn add trunk/$f
    svn st
    svn ci -m "file $f added to trunk/"
done
# + some dirs
mkdir trunk/dir1
> trunk/dir1/file4.txt
svn mkdir --parents trunk/dir1/file4.txt
svn st
svn ci -m "added trunk/dir1/file4.txt"

svn st && svn up && svn log -v .

#creating tag1
svn cp file://$SVN_REPO/trunk file://$SVN_REPO/tags/tag1 -m "tag1 created"
svn st && svn up && svn log -v .

#creating tag2
svn cp file://$SVN_REPO/trunk/dir1 file://$SVN_REPO/tags/tag2 -m "tag2 created"
svn st && svn up && svn log -v .

#show all tags
svn ls file://$SVN_REPO/tags
svn ls -vR file://$SVN_REPO/tags

echo "done"

