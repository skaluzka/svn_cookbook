#!/bin/bash
set -e


function doc() {
    cat <<EOF

********
play scenario:

whole "branches/" dir was accidentally deleted - traversing
trough tree after svn reverse merge
********

EOF
}

doc


WORK_DIR=$(pwd)
DIR_SUFFIX=$(basename $0 ".sh")
SVN_REPO=$WORK_DIR/SVN_REPO_${DIR_SUFFIX}
SVN_WORKSPACE=$WORK_DIR/SVN_WORKSPACE_${DIR_SUFFIX}

echo "cleaning dir, please wait..."
rm -rf $SVN_REPO $SVN_WORKSPACE

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${SVN_REPO} ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

#committing some changes to trunk...
cd $SVN_WORKSPACE/trunk
FILES="trunk_file1.txt trunk_file2.txt trunk_file3.txt trunk_file4.txt"
for f in $FILES; do
    date > $f
    svn add $f
    svn st
    svn ci -m "file $f added to trunk"
done
svn st && svn up && svn log -v .

#creating branches...
cd $SVN_WORKSPACE
svn cp -r2 trunk branches/branch1
svn st
svn ci -m "branch1 added"

svn cp -rHEAD trunk branches/branch2
svn st
svn ci -m "branch2 added"

svn up
svn log -v .

#committing some changes to branches...
cd $SVN_WORKSPACE/branches
BRANCHES="branch1 branch2"
for b in $BRANCHES; do
    date > $b/file.txt
    svn st
    svn add $b/file.txt
    svn ci -m "added file.txt to branch $b"
done;

svn up

#create unversioned tmp/ dir
mkdir -p $SVN_WORKSPACE/tmp/step1 $SVN_WORKSPACE/tmp/step2

#svn log with --stop-on-copy switch for branch1
svn log -v --stop-on-copy $SVN_WORKSPACE/branches/branch1 > $SVN_WORKSPACE/tmp/step1/branch1.log
echo "log for branch1:"
cat $SVN_WORKSPACE/tmp/step1/branch1.log

#svn log with --stop-on-copy switch for branch2
svn log -v --stop-on-copy $SVN_WORKSPACE/branches/branch2 > $SVN_WORKSPACE/tmp/step1/branch2.log
echo "log for branch2:"
cat $SVN_WORKSPACE/tmp/step1/branch2.log

#let's now delete branches/ dir
cd $SVN_WORKSPACE
svn del branches/
svn ci -m "\"copy/paste error\" :)"
svn up
svn log -v

#do svn reverse merge...
svn merge -r10:9 .
svn st
svn ci -m "branches resurrection"
svn up

#svn log with --stop-on-copy switch for branch1
svn log -v --stop-on-copy $SVN_WORKSPACE/branches/branch1 > $SVN_WORKSPACE/tmp/step2/branch1.log
echo "log for branch1:"
cat $SVN_WORKSPACE/tmp/step2/branch1.log

#svn log with --stop-on-copy switch for branch2
svn log -v --stop-on-copy $SVN_WORKSPACE/branches/branch2 > $SVN_WORKSPACE/tmp/step2/branch2.log
echo "log for branch2:"
cat $SVN_WORKSPACE/tmp/step2/branch2.log

echo "done"

